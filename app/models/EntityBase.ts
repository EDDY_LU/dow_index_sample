import { Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from 'typeorm';

export abstract class EntityBase {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ nullable: true })
    createdBy!: string;

    @Column()
    @CreateDateColumn()
    createdDate!: Date;

    @Column()
    @UpdateDateColumn()
    lastUpdateDate?: Date;
}