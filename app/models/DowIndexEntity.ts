import { Column, Entity } from 'typeorm';
import { EntityBase } from './EntityBase';

/**
 * The entity model of dataset aligned with those used in Dow Jones Index
 */
@Entity()
export class DowIndex extends EntityBase {
    /**
     * The yearly quarter (1 = Jan-Mar, 2=Apr-Jun)
     */
    @Column()
    quarter!: string;

    /**
     * The stock symbol
     */
    @Column()
    stock!: string;

    /**
     * The last business day of the work (typically a Friday)
     */
    @Column()
    date!: string;

    /**
     * The price of the stock at the beginning of the week
     */
    @Column()
    open!: string;

    /**
     * The highest price of the stock during the week
     */
    @Column()
    high!: string;

    /**
     * The lowest price of the stock during the week
     */
    @Column()
    low!: string;

    /**
     * The price of the stock at the end of the week
     */
    @Column()
    close!: string;

    /**
     * The number of shares of stock that traded hands in the week
     */
    @Column()
    volume!: number;

    /**
     * The percentage change in price throughout the week
     */
    @Column()
    percentChangePrice!: string;

    /**
     * The percentage change in the number of shares of stock that 
     * traded hands for this week compared to the previous week
     */
    @Column({ nullable: true })
    percentChangeVolumeOverLastWeek?: string;

    /**
     * The number of shares of stock that traded hands in the previous week
     */
    @Column({ nullable: true })
    previousWeeksVolume?: string;

    /**
     * The opening price of the stock in the following week
     */
    @Column()
    nextWeeksOpen!: string;

    /**
     * The closing price of the stock in the following week
     */
    @Column()
    nextWeeksClose!: string;

    /**
     * The percentage change in price of the stock in the following week 
     */
    @Column()
    percentChangeNextWeeksPrice!: string;

    /**
     * The number of days until the next dividend
     */
    @Column()
    daysToNextDividend!: number;

    /**
     * The percentage of return on the next dividend
     */
    @Column()
    percentReturnNextDividend!: string;
}