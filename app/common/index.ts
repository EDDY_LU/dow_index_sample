import { Router } from 'express';

type MdWrapper = ((router: Router) => void);

export const applyMiddleware = (
    middleware: MdWrapper[], 
    router: Router
) => {
    for (const wrapper of middleware) {
        wrapper(router);
    }
};