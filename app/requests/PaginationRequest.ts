export interface PaginationRequest {
    pageOffset: number;
    pageSize: number;
    orderBy: string;
    asc: boolean;
}