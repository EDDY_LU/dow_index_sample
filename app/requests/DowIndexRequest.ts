
export interface DowIndexRequest {
    stock: string;
    quarter: string;
    date: string;
    open: string;
    high: string;
    low: string;
    close: string;
    volume: number;
    percentChangePrice: string;
    percentChangeVolumeOverLastWeek: string;
    previousWeeksVolume: string;
    nextWeeksOpen: string;
    nextWeeksClose: string;
    percentChangeNextWeeksPrice: string;
    daysToNextDividend: number;
    percentReturnNextDividend: string;
} 