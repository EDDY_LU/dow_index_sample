import { ConnectionOptions } from 'typeorm';

export const connOptions: ConnectionOptions = {
    type: 'postgres',
    url: `postgresql://${process.env.POSTGRE_USER}:${process.env.POSTGRE_PASSWORD}@${process.env.POSTGRE_HOST}:5432/${process.env.POSTGRE_DB}`,
    synchronize: true,
    logging: ['query', 'error'],
    logger: 'file',
    entities: [
        __dirname + '/models/*{.ts,.js}'
    ],
    migrations: [
        __dirname + '/migrations/**/*{.ts, .js}'
    ],
    subscribers: [
        __dirname + '/subscriber/*{.ts, .js}'
    ],
    cli: {
        migrationsDir: 'app/migration'
    }
};