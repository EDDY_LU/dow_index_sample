import { getRepository, Repository } from 'typeorm';
import { DowIndex } from '../models/DowIndexEntity';
import { PaginationRequest } from '../requests/PaginationRequest';
import fs from 'fs';
import csv2json from 'csvtojson';
import { convertJsonData } from '../util/data-converter';
import { forEach } from 'lodash';

/**
 * Service to deal with Dow Jones Sample dataset
 */
export class DowIndexService {
	/**
	 * Return dataset based off of the stock input
	 * @param stock the stock symbol
	 * @param pagination 
	 */
	async getResultsByStock(stock: any, pagination?: PaginationRequest) {
		const repo: Repository<DowIndex> = getRepository(DowIndex);
		let query = repo.createQueryBuilder('dow').where('dow.stock=:stock', { stock: stock });
		query = pagination && pagination.orderBy ? query.orderBy(pagination.orderBy, pagination.asc ? 'ASC' : 'DESC') : query;
		query = pagination && pagination.pageOffset && pagination.pageSize ? query.skip(pagination.pageOffset).take(pagination.pageSize) : query;
		return await query.getMany();
	}

	/**
	 * Process the uploaded dataset via file upload
	 * @param file bulk dataset uploaded (assuming it's csv)
	 */
	async loadDataSetFrom(file: any) {
		if (!file) {
			throw new Error('Invalid file');
		}
		const data = fs.createReadStream(file.path, 'utf-8');
		const convertedData = await csv2json().fromStream(data);
		const payload = convertJsonData(convertedData);
		const repo = getRepository(DowIndex);
		forEach(payload, async data => {
			await repo.save(data);
		});
	}

	/**
	 * Store the new dataset
	 * @param data a new piece of dataset input
	 */
	async saveNewData(data: any) {
		const repo = getRepository(DowIndex);
		const newData: DowIndex = Object.assign(new DowIndex(), data);
		repo.save(newData);
	}
}
