import express from 'express';
import multer from 'multer';

export class FileHandlerService {
    private FILE_FORM_KEY = 'asset';
    private ASSET_FOLDER_NAME = 'uploads/';
    
    /**
     * Upload single file and temporarily store
     * it in ./uploads folder using a form fieldname
     * @param req
     * @param fieldName
     * @param filter 
     */ 
    uploadSingleFile(req: express.Request) {
        const multerSingle = multer({dest: this.ASSET_FOLDER_NAME}).single(this.FILE_FORM_KEY);
        return new Promise((resolve, reject) => {
            multerSingle(req, express.response, async (err: any) => {
                if (err) {
                    reject(err);
                }
                resolve();
            });
        });
    }

}