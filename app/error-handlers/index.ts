import { Router, Request, Response, NextFunction } from 'express';
import * as ErrorHandler from './errors/error-handler';

const handleClientErrors = (router: Router) => {
    router.use((err: Error, req: Request, res: Response, next: NextFunction) => {
        console.error(err.message, { stack: err.stack });
        ErrorHandler.clientError(err, res, next);
    });
};

const handleServerErrors = (router: Router) => {
    router.use((err: Error, req: Request, res: Response, next: NextFunction) => {
        console.error(err.message, { stack: err.stack });
        ErrorHandler.serverError(err, res, next);
    });
};

export default [handleClientErrors, handleServerErrors];