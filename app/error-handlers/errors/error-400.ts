import { HttpClientError } from './http-client-errors';

export class Http400Error extends HttpClientError {
    readonly statusCode = 400;
    constructor(message: string | object = 'Bad request') {
        super(message);
    }
}