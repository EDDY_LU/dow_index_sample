import { HttpClientError } from './http-client-errors';

export class Http401Error extends HttpClientError {
    readonly statusCode = 401;
    /**
     * HTTP 401 Error: Unauthorized Request
     */
    constructor(message: string | object = 'Unauthorized request') {
        super(message);
    }
}