import { Response, NextFunction } from 'express';
import { HttpClientError } from './http-client-errors';

export const clientError = (err: Error, res: Response, next: NextFunction) => {
    if (err instanceof HttpClientError) {
        console.warn(err);
        res.status(err.statusCode).json(err.message); 
    } else {
        next(err);
    }
};

export const serverError = (err: Error, res: Response, next: NextFunction) => {
    console.error(err);
    if (process.env.NODE_ENV === 'production') {
        res.status(500).send('Internal system error');
    } else {
        res.status(500).json(err.stack);
    }

};