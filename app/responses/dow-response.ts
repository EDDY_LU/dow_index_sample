export interface DowApiResponse {
    data?: any;
    total?: number;
}

export function createDefaultResponse(responseData?: any, size?: number): DowApiResponse {
    return {
        data: responseData,
        total: size
    };
}
