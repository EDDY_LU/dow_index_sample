/**
 * Swagger API response
 */
export interface ApiResponse {
    message: string;
    status: string | number;
    data?: any;
}

export function createApiResponse(message: string, status: string | number, data?: any): ApiResponse {
    return {
        message: message,
        status: status,
        data: data
    };
}