import { forOwn, camelCase } from 'lodash';

export function convertJsonData(json: Array<any>) {
    return json.map(item => {
        const result = {};
        forOwn(item, (v, k) => {
            Object.defineProperty(result, camelCase(k), {
                value: v,
                writable: true,
                enumerable: true,
                configurable: true
            });
        });
        return result;
    });
}