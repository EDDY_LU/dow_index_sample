import express from 'express';
import { createConnection } from 'typeorm';
import 'reflect-metadata';
import http from 'http';
import { applyMiddleware } from './common';
import middleware from './middleware';
import errHandlers from './error-handlers';
import { connOptions } from './db.options';
import { RegisterRoutes } from './routes';

const cors = require('cors');

process.on('uncaughtException', e => {
  console.log(`uncaughtException: ${e}`);
});

process.on('unhandledRejection', e => {
  console.log(`unhandledRejection: ${e}`);
});

createConnection(connOptions)
  .then(async conn => {
    const app = express();
    const host = process.env.SERVER_HOST || process.env.host;
    const port = process.env.SERVER_PORT || 3001;
    const env = process.env.NODE_ENV;

    applyMiddleware(middleware, app);
    app.use(cors());
    RegisterRoutes(app);
    applyMiddleware(errHandlers, app);
    const server = http.createServer(app);
    server.listen(port, () => {
      console.info(`${env} server up and running on ${host}:${port}`);
    });

  })
  .catch(error => console.error(error));
