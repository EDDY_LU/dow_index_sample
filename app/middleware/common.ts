import { Router } from 'express';
import cors from 'cors';
import parser from 'body-parser';
import compression from 'compression';
import methodoverride from 'method-override';
import helmet = require('helmet');

//options for cors midddleware
const options:cors.CorsOptions = {
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'X-Access-Token', 'allowedHeaders', 'Authorization'],
    credentials: true,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    origin: process.env.AL_FRONTEND_URL,
    preflightContinue: false,
    optionsSuccessStatus: 204
};


//enable pre-flight
export const handlePreflight = (router: Router) => router.options('*', cors(options));

//use cors middleware
export const handleCors = (router: Router) => router.use(cors(options));

export const putHelmet = (router: Router) => router.use(helmet({
    // options to set helmet
}));

export const handleBodyRequestParsing = (router: Router) => {
    //by default 100k allowed in the body. Expand to 10 MB
    router.use(parser.urlencoded({extended: true, limit: '10mb'}));
    router.use(parser.json({limit: '10mb'}));
};

export const handleCompression = (router: Router) => {
    router.use(compression());
};

export const handleMethodOverride = (router: Router) => {
    router.use(methodoverride());
};
