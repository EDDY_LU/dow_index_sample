import { Router } from 'express';
import swaggerUi = require('swagger-ui-express');
import * as swaggerDoc from '../swagger.json';

export const handleApiDocs = (router: Router) => {
    router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc, { explorer: true }));
};