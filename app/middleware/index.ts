import { handlePreflight, handleCors, putHelmet, 
         handleBodyRequestParsing, handleCompression,  } from './common';
import { handleApiDocs } from './swagger-doc';

export default [
    handlePreflight,
    handleCors, 
    putHelmet, 
    handleBodyRequestParsing, 
    handleCompression, 
    handleApiDocs
];