import * as express from 'express';
import { Controller, Route, Tags, Get, SuccessResponse, Response, Query, Post, Request, Body } from 'tsoa';
import { ApiResponse } from '../responses/api-response';
import { DowIndexService } from '../services/dow-index.service';
import { DowIndexRequest } from '../requests/DowIndexRequest';
import { FileHandlerService } from '../services/file-handler.service';
import { Http400Error } from '../error-handlers/errors/error-400';

@Route('dow-index-datasets')
@Tags('Dow Jones Index')
export class MainController extends Controller {
    private _dowIndexService: DowIndexService = new DowIndexService();
    private _fileHandler: FileHandlerService = new FileHandlerService();

    @Get()
    @SuccessResponse(200)
    @Response<ApiResponse>(400)
    public async getResultsByStock(@Query() stock: string, @Query() asc?: boolean, 
                                @Query() pageIndex?: number, @Query() pageSize?: number, 
                                @Query() sortBy?: string) {                           
        const paginationRequest = {
            pageOffset: pageIndex || 0,
            pageSize: pageSize || 10,
            orderBy: sortBy || 'quarter',
            asc: asc || true
        };
        return this._dowIndexService.getResultsByStock(stock, paginationRequest).catch(e => {
            this.setStatus(400);
            throw new Http400Error(e);
        });
    }

    @Post('/upload')
    @SuccessResponse(204)
    @Response<ApiResponse>(400)
    public async uploadDataSet(@Request() req: express.Request) {
        await this._fileHandler.uploadSingleFile(req).catch(err => {
            this.setStatus(400);
            throw new Http400Error(err);
        });
        await this._dowIndexService.loadDataSetFrom(req.file).then(data => {
            this.setStatus(204);
        }).catch(err => {
            this.setStatus(400);
            throw new Http400Error(err);
        });
    }
                  
    @Post()
    @SuccessResponse(204)
    @Response<ApiResponse>(400)
    public async addNewData(@Body() request: DowIndexRequest) {
        await this._dowIndexService.saveNewData(request).then(data => {
            this.setStatus(204);
        }).catch(err => {
            this.setStatus(400);
            throw new Http400Error(err);
        });
    }
}