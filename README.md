# Down Jones Index API

## Objective
The DowJonesIndex API is the backend framework to manage the REST API endpoints, controllers, business services and database models so that a client app can upload a bulk data set, query for data ticker and add a new record.

## Usage
Before starting the server, create a .env file at the root of the app and use <KEY>=<VALUE> pattern to add values for some variables specified below:
* NODE_ENV=development
* SERVER_PORT=3001

### Prerequisites

#### 1. Environment Variable
After run `npm install` in the app root folder, fill in the env values used for the target environment, which should look like below:
```
    NODE_ENV=<Your_Node_Environment>
    SERVER_HOST=<Your_Server_Host>
    SERVER_PORT=<Your_Server_Port>
    POSTGRE_HOST=<Your_Database_Host>
    POSTGRE_PORT=<Your_Database_Port>
    POSTGRE_USER=<Your_Database_User>
    POSTGRE_PASSWORD=<Your_Database_Password>
    POSTGRE_DB=<Your_Database_Name>
```

#### 2. Preconfig Database
In a local development environment, a Postgres Server user is required and set up. Create a local database named `dow_index_db` for the user. Refer to the `ConnectionOptions` in `index.ts` for more information.

#### 3. Intall & Run Packages
Run `npm run dev` for development purpose. 
Run `npm run build && npm start` to run a production build.